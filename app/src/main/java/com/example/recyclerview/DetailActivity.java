package com.example.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.example.recyclerview.Fragments.DetailFragment;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        // on récupère l'element passer en paramètre
        final String intitule_memo = getIntent().getStringExtra("memo_intitule");

        DetailFragment fragment = new DetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString("intitule_memo", intitule_memo);
        fragment.setArguments(bundle);
        // fragment manager :
        FragmentManager fragmentManager = getSupportFragmentManager();
        // transaction :
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.constraint_container_fragment, fragment, "premier_fragment");
        fragmentTransaction.commit();


    }
}
