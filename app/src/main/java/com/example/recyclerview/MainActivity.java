package com.example.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.recyclerview.Adapter.MemoAdapter;
import com.example.recyclerview.DTO.MemoDTO;
import com.example.recyclerview.Database.AppDatabase;
import com.example.recyclerview.Helper.ItemTouchHelperCallback;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public EditText editText;

    List<MemoDTO> listeMemos = new ArrayList<>();

    MemoAdapter memoAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Context context = getApplicationContext();
        // récuperation du champs de saisie
        editText = (EditText)findViewById(R.id.ajout_memo);
        // récupération du recycler view
        RecyclerView recyclerView = findViewById(R.id.libelle_memo);
        // Instanciation de la BDD
        AppDatabaseHelper.getDatabase(context);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        // Création d'un jeu de donnée
        /*AppDatabaseHelper.getDatabase(this).memosDAO().insert((new MemoDTO("Premier mémo")));
        AppDatabaseHelper.getDatabase(this).memosDAO().insert((new MemoDTO("Some mémo")));
        AppDatabaseHelper.getDatabase(this).memosDAO().insert((new MemoDTO("Encore un mémo")));
        AppDatabaseHelper.getDatabase(this).memosDAO().insert((new MemoDTO("Another one mémo")));
        AppDatabaseHelper.getDatabase(this).memosDAO().insert((new MemoDTO("Très jolie mémo")));
        AppDatabaseHelper.getDatabase(this).memosDAO().insert((new MemoDTO("Un beau mémo")));
        AppDatabaseHelper.getDatabase(this).memosDAO().insert((new MemoDTO("Ca fait long mémo")));
        AppDatabaseHelper.getDatabase(this).memosDAO().insert((new MemoDTO("Le dernier des derniers mémo")));*/

        // Récupère la liste des mémos en base
        List<MemoDTO> listeMemos = AppDatabaseHelper.getDatabase(this)
                .memosDAO()
                .getListeCourses();

        // Récupèreation des données dans les SharedPreferences
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(this);
        String intitule = preferences.getString("lastItemClicked", "");
        if(!intitule.equals("")) {
            afficherInformationToast(intitule);
        }


        memoAdapter = new MemoAdapter(listeMemos, context);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(
                new ItemTouchHelperCallback(memoAdapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);
        recyclerView.setAdapter(memoAdapter);

    }

    public void afficherInformationToast(String info){
        Toast toast = Toast.makeText(getApplicationContext(),info,Toast.LENGTH_SHORT);
        toast.show();
    }

    public void insererMemo(View v) {
        MemoDTO m = new MemoDTO(editText.getText().toString());
        editText.setText("");
        memoAdapter.ajouterMemo(m);

    }



}
