package com.example.recyclerview.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.recyclerview.DTO.MemoDTO;

import java.util.List;

@Dao
public abstract class MemosDAO {

    @Query("SELECT * FROM memos")
    public abstract List<MemoDTO> getListeCourses();

    @Query("SELECT COUNT(*) FROM memos WHERE intitule = :intitule")
    public abstract long countCoursesParIntitule(String intitule);

    @Insert
    public abstract void insert(MemoDTO... courses);

    @Update
    public abstract void update(MemoDTO... courses);

    @Delete
    public abstract void delete(MemoDTO... courses);
}
