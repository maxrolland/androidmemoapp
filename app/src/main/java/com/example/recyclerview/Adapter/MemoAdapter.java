package com.example.recyclerview.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recyclerview.DTO.MemoDTO;
import com.example.recyclerview.DetailActivity;
import com.example.recyclerview.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.util.Collections;
import java.util.List;

import cz.msebera.android.httpclient.entity.mime.Header;

public class MemoAdapter extends RecyclerView.Adapter<MemoAdapter.MemoViewHolder> {

    private List<MemoDTO> listeMemos;

    public Context context;

    AsyncHttpClient client = new AsyncHttpClient();


    public MemoAdapter (List<MemoDTO> listeMemos,Context context) {
        this.listeMemos = listeMemos;
        this.context = context;
    }

    public boolean onItemMove(int positionDebut, int positionFin)
    {
        Collections.swap(listeMemos, positionDebut, positionFin);
        notifyItemMoved(positionDebut, positionFin);
        return true;
    }


    // Appelé une fois à la suppression.
    public void onItemDismiss(int position)
    {
        if (position > -1)
        {
            listeMemos.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void ajouterMemo(MemoDTO memoDTO){
        listeMemos.add(0,memoDTO);
        this.notifyItemInserted(0);
    }

    @NonNull
    @Override
    public MemoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.memo_list_item, parent,false);
        return new MemoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MemoViewHolder holder, int position) {
        holder.textViewLibelleMemo.setText(listeMemos.get(position).intitule);
    }

    @Override
    public int getItemCount() {
        return listeMemos.size();
    }

    public class MemoViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewLibelleMemo;

        public MemoViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewLibelleMemo = itemView.findViewById(R.id.libelle_item_memo);
            textViewLibelleMemo.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    MemoDTO memo = listeMemos.get(getAdapterPosition());
                    Toast toast = Toast.makeText(context,memo.getIntitule(), Toast.LENGTH_SHORT);
                    toast.show();

                    // Call POST
                    RequestParams requestParams = new RequestParams();
                    requestParams.put("memo", memo.getIntitule());

                    client.post("http://httpbin.org/post", requestParams, new AsyncHttpResponseHandler()
                    {
                        @Override
                        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                            String retour = new String(responseBody);
                            Log.i("RetourSuccessPost", retour);
                            // on affiche le resultat dans un toast
                            Toast toast = Toast.makeText(context,retour, Toast.LENGTH_SHORT);
                            toast.show();
                        }

                        @Override
                        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                            String retour = new String(responseBody);
                            Log.i("RetourErrorPost", retour);
                        }

                    });


                    // Sauvergarde dans les sharedPreferences
                    stockageSharedPreferences(memo);


                    // Changement de page pour accéder au fragment
                    Intent intent = new Intent(context, DetailActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("memo_intitule", memo.getIntitule());
                    context.startActivity(intent);
                }
            });



        }


    }

    private void stockageSharedPreferences(MemoDTO memo) {
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("lastItemClicked",memo.getIntitule());
        editor.apply();
    }


}


