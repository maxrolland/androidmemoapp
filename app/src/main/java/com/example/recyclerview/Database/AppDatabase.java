package com.example.recyclerview.Database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.recyclerview.DAO.MemosDAO;
import com.example.recyclerview.DTO.MemoDTO;

@Database(entities = {MemoDTO.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase
{
    public abstract MemosDAO memosDAO();

}
