package com.example.recyclerview.DTO;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "memos")
public class MemoDTO {

    @PrimaryKey(autoGenerate = true)
    public long memosId = 0;
    public String intitule;


    @Ignore
    public boolean selectionne;

    public MemoDTO() {}

    public MemoDTO(String intitule)
    {
        this.intitule = intitule;
    }

    public long getMemosId() {
        return memosId;
    }

    public void setMemosId(long memosId) {
        this.memosId = memosId;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public boolean isSelectionne() {
        return selectionne;
    }

    public void setSelectionne(boolean selectionne) {
        this.selectionne = selectionne;
    }


}
